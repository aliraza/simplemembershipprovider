# README #

This SimpleMembershipProvider is written in C#.NET and tested against both SQL Server and MySQL. It is assumed that this will work against other relational databases also.

### What is this repository for? ###
* Version 1.0
.NET Simple Membership Provider was developed to work against SQL Server database only. The need arisen when we had to migrate our data source from SQL Server to MySQL. The simple membership provider that was initially implemented against SQL Server in one of our projects did not remain functional against MySQL. 

* This project is the slight modification of the original implementation of .NET Simple Membership Provider.

### How do I get set up? ###
The Unco.SimpleMembershipProvider is extremely easy to setup. You can either add this project to your existing solution or add just a reference dll of this project to your .NET solution. Rest of the setup is exactly the same as that of .NET Simple Membership Provider that is included in WebMatrix.Data namespace. 

* Add a reference of Unco.SimpleMembershipProvider to your project. 
* In your web.config along with connection string add the membership provider within <system.web> tag boundary as follows

```
#!c#

<membership defaultProvider="SimpleMembershipProvider">
      <providers>
        <clear />
        <remove name="SimpleMembershipProvider" />
        <add name="SimpleMembershipProvider"
             type="Unco.MembershipProvider.SimpleMembershipProvider, Unco.MembershipProvider"
             connectionStringName="DefaultConnection" 
             enablePasswordRetrieval="false" 
             enablePasswordReset="true" 
             requiresQuestionAndAnswer="true" 
             applicationName="/" 
             requiresUniqueEmail="false" 
             passwordFormat="Clear" 
             maxInvalidPasswordAttempts="5" 
             minRequiredPasswordLength="7" 
             minRequiredNonalphanumericCharacters="1" 
             passwordAttemptWindow="10" 
             passwordStrengthRegularExpression=""/>
      </providers>
    </membership>
```

* Add a connection string as follows:


```
#!c#

<connectionStrings>
	  <add name="DefaultConnection"
		   connectionString="server=127.0.0.1;user id=mysql;password=mysqlpwd;database=DatabaseName;persistsecurityinfo=True"
		   providerName="MySql.Data.MySqlClient"
		    />
  </connectionStrings>
```

* Replace WebMatrix.WebData namespace with Unco.MembershipProvider in your code file where you will initialize the SimpleMembershipProvider. The code you previously used to work with Sql Server database may look like this


```
#!c#

WebSecurity.InitializeDatabaseConnection("MySqlDefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
```

* replace this code with the following:


```
#!c#

WebSecurity.InitializeDatabaseConnection("MySqlDefaultConnection", DatabaseType.MySql, "UserProfile", "UserId", "UserName", autoCreateTables: true);
```

where DatabaseType is an enum


```
#!c#

public enum DatabaseType
{
        SqlServer, 
        MySql, 
        Oracle, 
        Access
}
```
To understand how this enum is utilised you will have to investigate the code a bit. In short this is to identify the database and add relevant attributes to the SQL Queries. 

Please let me know if you find any issues with this implementation.